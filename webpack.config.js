var path = require('path')
var webpack = require('webpack')



module.exports = {
    entry: {
        "lt-good-table" : './src/components/Table.vue',
    },
    output: {
        filename: '[name].js',
        chunkFilename: '[id].[chunkhash].js',
        filename: 'lt-good-table.js',
        library: 'lt-good-table',
        libraryTarget: 'umd',
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.scss$/,
                loader: ['css-loader', 'sass-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: './images/[name].[ext]?[hash]'
                },
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                loader: 'file-loader'
            }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue'
        }
    },
    externals: {
        vue: "'vue'"
    },
    devServer: {
        historyApiFallback: true,
        noInfo: true,
        disableHostCheck: true,
    },
    devtool: '#eval-source-map',
    plugins: [

    ]
}

var commonChunksDirectives = [
]

if (process.env.NODE_ENV === 'testing') {
	module.exports.devtool = '#inline-source-map'
} else if (process.env.NODE_ENV === 'local') {
    var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
    module.exports.plugins = (module.exports.plugins || [])
} else {
    module.exports.devtool = '#source-map'
    // http://vue-loader.vuejs.org/en/workflow/production.html
    module.exports.plugins = (module.exports.plugins || []).concat([
        
        // Compress extracted CSS. We are using this plugin so that possible
        // duplicated CSS from different components can be deduped.),
    ])
}